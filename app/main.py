import cv2
import os
import functions.contours_functions as fn_find
import functions.draw_functions as fn_draw
import functions.html_functions as fn_html


# find and draw edges on image based on Canny, and Dilated functions in OpenCv
def find_edges(file_path):
    img = cv2.imread(file_path, cv2.IMREAD_GRAYSCALE)
    if img is None:
        print('Fail load file!')
        return None
    img = cv2.resize(img, (0, 0), fx=0.2, fy=0.2)
    edges = fn_find.auto_canny(img)
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))  # TODO: poczytac o tym
    dilated = cv2.dilate(edges, kernel, iterations=1)  # dilate
    return dilated


# find contours based on edges drawn on image
def find_contours(img):
    height, width = img.shape
    cont_img = img.copy()
    img2, contours, hierarchy = cv2.findContours(cont_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)  # get contours
    contours, hierarchy = fn_find.validate_contours(contours, hierarchy[0], img)
    new_img = fn_draw.create_img(width, height)
    fn_draw.draw_rectangles_with_text(contours, hierarchy, new_img)
    return contours, hierarchy, new_img


# get filename, and path to folder
def get_file_info(path):
    path_dir = os.path.dirname(path) + '/'
    base = os.path.basename(path)
    file_name = os.path.splitext(base)[0]
    return path_dir, file_name


def main_function(path):
    edges = find_edges(path)
    cnts, hierarchy, img = find_contours(edges)
    html_hierarchy = []
    fn_html.traversal_tree(html_hierarchy, 0, cnts, hierarchy)
    print('Is ok!')
    path_dir, file_name = get_file_info(path)
    fn_draw.show_img(img, True, '{0}/{1}_edges.png'.format(path_dir, file_name))


# ** Main program **
main_function('../images/01.jpg')
# find_operation('../images/02.jpg')
