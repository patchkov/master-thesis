# hierarchy [next, previous, first_child, parent]
import cv2
import math


def traversal_tree(array_new_order, index, cnts, hierarchy):
    cnt_in_hierarchy = hierarchy[index]
    parent = cnt_in_hierarchy[3]
    if not is_single_node(cnt_in_hierarchy) and cnt_in_hierarchy[1] == -1:
        check_nodes_on_level(array_new_order, parent, index, cnts, hierarchy)
    if cnt_in_hierarchy[2] != -1:
        traversal_tree(array_new_order, cnt_in_hierarchy[2], cnts, hierarchy)
    if cnt_in_hierarchy[0] != -1:
        traversal_tree(array_new_order, cnt_in_hierarchy[0], cnts, hierarchy)


def check_nodes_on_level(array_new_order, parent, index, cnts, hierarchy):
    array_sum_of_row_width = sum_width_on_level(index, cnts, hierarchy)
    array_new_order_on_level = []
    cnt = cnts[index]
    cnt_in_hierarchy = hierarchy[index]
    current_sum_row_width = 0
    while True:
        x, y, w, h = cv2.boundingRect(cnt)
        next_cnt = cnts[cnt_in_hierarchy[0]] if cnt_in_hierarchy[0] != -1 else None
        previous_cnt = cnts[cnt_in_hierarchy[1]] if cnt_in_hierarchy[1] != -1 else None
        current_sum_row_width = get_current_row_width(array_sum_of_row_width, index) or current_sum_row_width
        bootstrap_column_width = current_sum_row_width / 12  # width needs to create one bootstrap column
        array_new_order_on_level.append({'y': y, 'x': x, 'index': index,
                                         'width': bootstrap_width_for_cnt(bootstrap_column_width, cnt, previous_cnt,
                                                                          next_cnt)})
        if cnt_in_hierarchy[0] == -1:
            break
        index = cnt_in_hierarchy[0]
        cnt = cnts[index]
        cnt_in_hierarchy = hierarchy[index]
    cnts_sort(array_new_order_on_level)
    children = [{cnt['index']: cnt['width']} for cnt in array_new_order_on_level]
    array_new_order.append({'id': parent, 'children': children})


def sum_width_on_level(index, cnts, hierarchy):
    sum_width_array = {}
    sum_of_width = 0  # sum of all elements in rows.
    cnt = cnts[index]
    cnt_in_hierarchy = hierarchy[index]
    first_cnts_in_row = index
    while True:
        # rectangle tuple: x, y, w, h
        x, y, w, h = cv2.boundingRect(cnt)
        sum_of_width += w
        if math.fabs(h - cv2.boundingRect(cnts[cnt_in_hierarchy[0]])[3]) > 10:  # Check if next cnt is in the same row
            sum_width_array[first_cnts_in_row] = sum_of_width
            first_cnts_in_row = cnt_in_hierarchy[0]
            sum_of_width = 0
        if cnt_in_hierarchy[0] == -1:
            sum_width_array[first_cnts_in_row] = sum_of_width
            break
        index = cnt_in_hierarchy[0]
        cnt = cnts[index]
        cnt_in_hierarchy = hierarchy[index]
    return sum_width_array


def get_current_row_width(array_sum_of_row_width, index):
    if index in array_sum_of_row_width:
        return array_sum_of_row_width[index]


def bootstrap_width_for_cnt(bootstrap_unit, cnt, previous_cnt, next_cnt):
    rect = cv2.boundingRect(cnt)
    previous_rect = cv2.boundingRect(previous_cnt)
    next_rect = cv2.boundingRect(next_cnt)
    if previous_cnt is None and math.fabs(rect[3] - next_rect[3]) > 10:
        return 12
    elif next_cnt is None and math.fabs(rect[3] - previous_rect[3]) > 10:
        return 12
    elif math.fabs(rect[3] - next_rect[3]) > 10 and math.fabs(rect[3] - previous_rect[3]) > 10:
        return 12
    return round(rect[2] / bootstrap_unit)


def cnts_sort(cnts):
    for i in range(len(cnts)):
        for j in range(i + 1, len(cnts)):
            compare_cnts(cnts, i, j)


def compare_cnts(cnts, i, j):
    if math.fabs(cnts[i]['y'] - cnts[j]['y']) > 10:
        if cnts[j]['y'] < cnts[i]['y']:
            cnts[j], cnts[i] = cnts[i], cnts[j]
    else:
        if cnts[j]['x'] < cnts[i]['x']:
            cnts[j], cnts[i] = cnts[i], cnts[j]


# check if node is single (without children and next sibling)
def is_single_node(cnt_in_hierarchy):
    return cnt_in_hierarchy[0] == -1 and \
           cnt_in_hierarchy[1] == -1 and \
           cnt_in_hierarchy[2] == -1
