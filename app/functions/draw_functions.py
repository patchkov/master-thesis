import cv2
import numpy as np
from matplotlib import pyplot as plt
import functions.html_functions as fn_html


# show image using plot library and save file
def show_img(img, is_save=False, file_path=""):
    plt.imshow(img, cmap='gray', interpolation='bicubic')
    plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
    plt.show()
    if is_save:
        cv2.imwrite(file_path, img)


# draw rectangles from contours
def draw_rectangles(cnts, hierarchy, img):
    for index, cnt in enumerate(cnts):
        if not fn_html.is_single_node(hierarchy[index]):
            x, y, w, h = cv2.boundingRect(cnt)
            cv2.rectangle(img, (x, y), (x + w, y + (h - 10)), (255, 255, 255), 1)


# draw rectangles from contours with their id as text
def draw_rectangles_with_text(cnts, hierarchy, img):
    font = cv2.FONT_HERSHEY_SIMPLEX
    for index, cnt in enumerate(cnts):
        if not fn_html.is_single_node(hierarchy[index]):
            x, y, w, h = cv2.boundingRect(cnt)
            cv2.putText(img, str(index), (int(x), int(y)), font, 1, (0, 0, 255), 1)
            cv2.rectangle(img, (x, y), (x + w, y + (h - 10)), (255, 255, 255), 1)


# create new, blank image with width and height as parameters
def create_img(width, height):
    blank_image = np.zeros((height, width, 3), np.uint8)
    return blank_image
