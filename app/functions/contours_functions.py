import cv2


# find edges using Canny with calculated parameters based on image
def auto_canny(image, sigma=0.45):
    # compute the median of the single channel pixel intensities
    v = 255
    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)
    # return the edged image
    return edged


# validate contours and remove too small and other than rectangle
def validate_contours(cnts, hierarchy, img):
    img_h, img_w = img.shape
    temp_cnts = []
    validate_cnts = []
    validate_hierarchy = []
    for index, cnt in enumerate(cnts):
        x, y, w, h = cv2.boundingRect(cnt)
        if (len(cnt)) >= 4 and w >= img_w * 0.05 and h >= img_h * 0.05:
            temp_cnts.append({'id': index, 'points': cnt})
            validate_cnts.append(cnt)
            validate_hierarchy.append(hierarchy[index])
        else:
            remove_contour_in_hierarchy(hierarchy, index)
    set_new_index(temp_cnts, validate_hierarchy)
    return validate_cnts, validate_hierarchy


def set_new_index(valid_cnts, valid_hierarchy):
    for i, cnt in enumerate(valid_cnts):
        for hierarchy_node in valid_hierarchy:
            for k in range(0, 4):
                if hierarchy_node[k] == cnt['id']:
                    hierarchy_node[k] = i


# remove contour from hierarchy tree. [next, previous, first_child, parent]
def remove_contour_in_hierarchy(hierarchy, index):
    current_cnt_in_hierarchy = hierarchy[index]
    # remove for horizontal level (siblings)
    if current_cnt_in_hierarchy[0] > -1 and current_cnt_in_hierarchy[1] > -1:
        next_cnt = hierarchy[current_cnt_in_hierarchy[0]]
        prev_cnt = hierarchy[current_cnt_in_hierarchy[1]]
        next_cnt[1] = current_cnt_in_hierarchy[1]
        prev_cnt[0] = current_cnt_in_hierarchy[0]
    elif current_cnt_in_hierarchy[0] > -1:
        next_cnt = hierarchy[current_cnt_in_hierarchy[0]]
        next_cnt[1] = -1
    elif current_cnt_in_hierarchy[1] > -1:
        prev_cnt = hierarchy[current_cnt_in_hierarchy[1]]
        prev_cnt[0] = -1
    # remove for vertical level (child <-> parent)
    if current_cnt_in_hierarchy[3] > -1 and current_cnt_in_hierarchy[1] == -1:
        parent = hierarchy[current_cnt_in_hierarchy[3]]
        if current_cnt_in_hierarchy[0] > -1:
            parent[2] = current_cnt_in_hierarchy[0]
        else:
            parent[2] = -1


